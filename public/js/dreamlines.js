/*
  * COPYRIGHT SOURCE CODE &COPY; RUPINDER SINGH SETHI M: +91-9915455521
  *
  * PLATFORM: NODEJS/EXPRESS/LOKIJS
  *
  * DEVELOPER NAME: RUPINDER SINGH SETHI / SR. NODE-JS/ANGULAR-JS/WEBRTC/SOCKET-IO/FULL STACK DEVELOPER
  *
  * DEVELOPER PROFILE: https://in.linkedin.com/in/rup-sethi-5753a3b4
  *
  * LOCATION: CHANDIGARH - INDIA
  *
  * DATE: 22 JUNE 2016
  *
  * CONFIDENTIALITY NOTICE: This source code file contains confidential information
  * that is legally privileged. Do not copy/re-use/re-distribute this file. If you
  * are not the intended recipient/user of this file, any disclosure, copying, or use
  * of any of the transmitted information is STRICTLY PROHIBITED. If you have received
  * this file in error, please immediately notify this to info@wegile.com and destroy
  * the original transmission/files and its attachments without reading or saving them.
  * Thank you.
*/
$("a.switch-tabs").on("click",function(){
	var id = $(this).attr("href").replace("#","");
	var data = $(this).data();
	$(".api-heading").html($(this).html());
	$(".api-action").html(id);
	$(".api-liveaction").html(data.live);
	$(".api-testaction").html(data.test);
	if(parseInt(data.length)==0)
		$(".row.paramsrow").hide();
	else
		$(".row.paramsrow").show();
	
	var testurl = data.test.replace("<b>","").replace("</b>","");
	$("button.testhereBtn").attr("onclick","window.open('api/"+testurl+"','DreamLines APIs Demonstration','width=800,height=600,toolbar=0,menubar=0,location=0,scrollbars=1');");
});


$(document).ready(function() {
	$("a.switch-tabs").eq(0).trigger('click');
});