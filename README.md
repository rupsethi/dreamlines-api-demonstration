# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* DreamLines Demo API's Demonstration
* Version: 1.0.0
* NodeJS Server Side Scripting for RESTful API's Development
* MVC Structured Application

### How do I get set up? ###

* Download and Install NodeJS Package from [here](https://nodejs.org/en/download/) for respective Operating System.
* Test NodeJS To see if Node is installed properly,
* Test NPM.
* Create a folder on any specific path e.g NodejsProject and makesure to allow Read/Write permissions to that folder.
* Clone the repo via GIT "git clone https://rupsethi@bitbucket.org/rupsethi/dreamlines-api-demonstration.git".
* Open the Commandline/Terminal and navigate to the folder (e.g NodejsProject) root where application source code is setup. 
* Now as a final Step run command "node app.js".
* If command executed successfully it will print a message like "Express server listening on port 3000".
* Keep the terminal open and command in running mode.
* Now open browser on same machine where running command and hit the link [http://localhost:3000](http://localhost:3000).
* Application interface will be loaded on browser.

### Debugging guidelines ###

* App Debugging via command "node --debug app.js". This will run the app in debugging mode and you can see the console logs in the terminal/commandline. 

### Dynamic Data Load From CSV ###

* This NodeJS App has the ability to dynamically load the data from CSV for API operations.
* Replace/Update the 'airport.csv' file with any new updated data it will automatically reflected in the api response. But make sure the name of the CSV should always be 'airport.csv' and it must have atleast those fields to which apis are mapped otherwise API's will crash if csv file does not contain the columns API requesting.
* After updating CSV file you need to restart the app again with "node app.js" command to reflect the updated data. 

### Who do I talk to? ###

* [Rup Sethi](https://in.linkedin.com/in/rup-sethi-5753a3b4)
* Email: sethi.rup@gmail.com
* Skype: sethi.rup (Rup Sethi)
* Mobile: +91-9915455521