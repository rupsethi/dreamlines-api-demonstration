/*
 * COPYRIGHT SOURCE CODE &COPY; RUPINDER SINGH SETHI M: +91-9915455521
 *
 * PLATFORM: NODEJS/EXPRESS/LOKIJS
 *
 * DEVELOPER NAME: RUPINDER SINGH SETHI / SR. NODE-JS/ANGULAR-JS/WEBRTC/SOCKET-IO/FULL STACK DEVELOPER
 *
 * DEVELOPER PROFILE: https://in.linkedin.com/in/rup-sethi-5753a3b4
 *
 * LOCATION: CHANDIGARH - INDIA
 *
 * DATE: 22 JUNE 2016
 *
 * CONFIDENTIALITY NOTICE: This source code file contains confidential information
 * that is legally privileged. Do not copy/re-use/re-distribute this file. If you
 * are not the intended recipient/user of this file, any disclosure, copying, or use
 * of any of the transmitted information is STRICTLY PROHIBITED. If you have received
 * this file in error, please immediately notify this to info@wegile.com and destroy
 * the original transmission/files and its attachments without reading or saving them.
 * Thank you.
 */
var path = require('path'), // initializing path module for better directory/file management
	fs = require('fs'); // File I/O operationa module initialized

module.exports.controller = function (app) {
	/*
	 * METHOD: HOME PAGE
	 * ACCEPTS CALLS: GET ONLY
	 * DATABASE INVOLVED: FALSE
	 * STORED PROCEDURE: FALSE
	 * SP INPUT PARAMETERS: FALSE
	 * RETURN TYPE: VIEW RENDER FOR HOME PAGE
	 */
	app.get('/', app.get('middleware'), function (req, res) {
		res.sendFile(path.join(req.rootdir, '/public', 'index.html'));
	});

	/*
	 * METHOD: ALL API'S ROOT DISPATCHER
	 * ACCEPTS CALLS: GET ONLY
	 * DATABASE INVOLVED: FALSE
	 * STORED PROCEDURE: FALSE
	 * SP INPUT PARAMETERS: FALSE
	 * RETURN TYPE: VIEW RENDER FOR ALL API'S DETAIL
	 */
	app.get('/apis', app.get('middleware'), function (req, res) {
		fs.readFile('./apis.json', 'utf8', function (err, data) {
			if (err) {
				res.send({
					status: 'ERROR',
					message: "Unable to load API's detail due to server error."
				});
				res.end();
			}
			return res.render('index', {
				appUrl: req.baseurl,
				list: JSON.parse(data)
			});
		});
	});
}