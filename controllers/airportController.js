/*
 * COPYRIGHT SOURCE CODE &COPY; RUPINDER SINGH SETHI M: +91-9915455521
 *
 * PLATFORM: NODEJS/EXPRESS/LOKIJS
 *
 * DEVELOPER NAME: RUPINDER SINGH SETHI / SR. NODE-JS/ANGULAR-JS/WEBRTC/SOCKET-IO/FULL STACK DEVELOPER
 *
 * DEVELOPER PROFILE: https://in.linkedin.com/in/rup-sethi-5753a3b4
 *
 * LOCATION: CHANDIGARH - INDIA
 *
 * DATE: 22 JUNE 2016
 *
 * CONFIDENTIALITY NOTICE: This source code file contains confidential information
 * that is legally privileged. Do not copy/re-use/re-distribute this file. If you
 * are not the intended recipient/user of this file, any disclosure, copying, or use
 * of any of the transmitted information is STRICTLY PROHIBITED. If you have received
 * this file in error, please immediately notify this to info@wegile.com and destroy
 * the original transmission/files and its attachments without reading or saving them.
 * Thank you.
 */
var db = require('../models/connection'), // Loki Database Intance created for DB operations
	helper = require('../helpers/GlobalHelper'); // Custom Helper initialized for sorting purpose

module.exports.controller = function (app) {
	/*
	 * METHOD: ALL API'S ROOT DISPATCHER
	 * ACCEPTS CALLS: GET ONLY
	 * DATABASE INVOLVED: TRUE
	 * STORED PROCEDURE: FALSE
	 * INPUT PARAMETERS: airport OR all (Collects Stats and Reviews)
	 * RETURN TYPE: JSON ARRAY
	 */
	app.get('/api/:airport/:type', function (req, res) {
		var airport = req.params.airport;
		var type = req.params.type;
		if (airport.trim() == "" || type.trim() == "") {
			res.send({
				status: "ERROR",
				message: "Invalid API call."
			});
			res.end();
		}

		var collection = 'airports';
		var airports = db.getCollection(collection);
		var resultset;
		if (airports.count() == 0) {
			resultset = {
				status: "OK",
				message: "Please restart nodejs application to load CSV or CSV data is empty/invalid."
			};
		} else {
			if (airport == "all" && type == "stats") {
				resultset = airports.mapReduce(function (obj) {
					return obj.airport_name;
				}, function (values) {
					var indexArr = [],
						dataArr = [];
					values.forEach(function (value) {
						if (!indexArr[value]) {
							dataArr.push({
								airport_name: value,
								count_of_reviews: 1
							});
							indexArr[value] = dataArr.length;
						} else
							dataArr[parseInt(indexArr[value]) - 1]['count_of_reviews'] += 1;
					});

					return dataArr.sort(helper.sortBy({
						name: 'count_of_reviews',
						primer: parseInt,
						reverse: true
					}, 'airport_name'));
				});
			} else if (type == "stats") {
				resultset = airports.mapReduce(function (obj) {
					var returnObj = {
						airport_name: obj.airport_name,
						overall_rating: obj.overall_rating,
						recommended: obj.recommended
					};
					return (obj.airport_name.toString() == airport.toString()) ? returnObj : null;
				}, function (values) {
					var dataObj = {
						airport_name: airport.toString(),
						count_of_reviews: 0,
						average_overall_rating: 0,
						count_of_recommendations: 0
					};
					var avg_rating = 0;
					values.forEach(function (value) {
						if (value != null) {
							dataObj.count_of_reviews += 1;
							dataObj.count_of_recommendations += (value.recommended) ? parseInt(value.recommended) : 0;
							avg_rating += (value.overall_rating) ? parseInt(value.overall_rating) : 0;
						}
					});

					if (parseInt(dataObj.count_of_reviews) > 0)
						dataObj.average_overall_rating = (avg_rating / dataObj.count_of_reviews).toFixed(2);

					return dataObj;
				});
			} else if (type == "reviews") {
				resultset = airports.mapReduce(function (obj) {
					var returnObj = {
						overall_rating: obj.overall_rating,
						date: obj.date,
						content: obj.content,
						author_country: obj.author_country,
						recommended: obj.recommended
					};
					return (obj.airport_name.toString() == airport.toString()) ? returnObj : null;
				}, function (values) {
					var dataArr = [];
					values.forEach(function (value) {
						if (value != null && value.overall_rating > 2) {
							var dataObj = {
								overall_rating: (value.overall_rating) ? value.overall_rating : 0,
								date: (value.date) ? value.date : '',
								author_country: (value.author_country) ? value.author_country : '',
								content: (value.content) ? value.content : '',
								recommendation: (value.recommended) ? parseInt(value.recommended) : 0
							};
							dataArr.push(dataObj);
						}
					});
					return dataArr.sort(helper.sortBy({
						name: 'date',
						reverse: true
					}));
				});
			} else {
				resultset = {
					status: "ERROR",
					message: "Invalid API call."
				};
			}
		}
		res.send(resultset);
		res.end();
	});
}