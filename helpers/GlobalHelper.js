/*
 * COPYRIGHT SOURCE CODE &COPY; RUPINDER SINGH SETHI M: +91-9915455521
 *
 * PLATFORM: NODEJS/EXPRESS/LOKIJS
 *
 * DEVELOPER NAME: RUPINDER SINGH SETHI / SR. NODE-JS/ANGULAR-JS/WEBRTC/SOCKET-IO/FULL STACK DEVELOPER
 *
 * DEVELOPER PROFILE: https://in.linkedin.com/in/rup-sethi-5753a3b4
 *
 * LOCATION: CHANDIGARH - INDIA
 *
 * DATE: 22 JUNE 2016
 *
 * CONFIDENTIALITY NOTICE: This source code file contains confidential information
 * that is legally privileged. Do not copy/re-use/re-distribute this file. If you
 * are not the intended recipient/user of this file, any disclosure, copying, or use
 * of any of the transmitted information is STRICTLY PROHIBITED. If you have received
 * this file in error, please immediately notify this to info@wegile.com and destroy
 * the original transmission/files and its attachments without reading or saving them.
 * Thank you.
 */
module.exports = {
	/*
	 * METHOD: Allows Multi Fields Sort for Array of Objects
	 * ACCEPTS CALLS: Function Call
	 * DATABASE INVOLVED: False
	 * STORED PROCEDURE: False
	 * INPUT PARAMS: 
	 * field: single/multiple field as array
	 * reverse: Boolean For Sorting Purpose
	 * primer: String/DOM Function name For filteration field
	 * RETURN TYPE: Array of Object
	 */
	sortBy: function (field, reverse, primer) {
		var fields = [].slice.call(arguments),
			n_fields = fields.length;

		return function (A, B) {
			var a, b, field, key, primer, reverse, result;
			for (var i = 0, l = n_fields; i < l; i++) {
				result = 0;
				field = fields[i];

				key = typeof field === 'string' ? field : field.name;

				a = A[key];
				b = B[key];

				if (typeof field.primer !== 'undefined') {
					a = field.primer(a);
					b = field.primer(b);
				}

				reverse = (field.reverse) ? -1 : 1;

				if (a < b) result = reverse * -1;
				if (a > b) result = reverse * 1;
				if (result !== 0) break;
			}
			return result;
		}
	}
}