/*
 * COPYRIGHT SOURCE CODE &COPY; RUPINDER SINGH SETHI M: +91-9915455521
 *
 * PLATFORM: NODEJS/EXPRESS/LOKIJS
 *
 * DEVELOPER NAME: RUPINDER SINGH SETHI / SR. NODE-JS/ANGULAR-JS/WEBRTC/SOCKET-IO/FULL STACK DEVELOPER
 *
 * DEVELOPER PROFILE: https://in.linkedin.com/in/rup-sethi-5753a3b4
 *
 * LOCATION: CHANDIGARH - INDIA
 *
 * DATE: 22 JUNE 2016
 *
 * CONFIDENTIALITY NOTICE: This source code file contains confidential information
 * that is legally privileged. Do not copy/re-use/re-distribute this file. If you
 * are not the intended recipient/user of this file, any disclosure, copying, or use
 * of any of the transmitted information is STRICTLY PROHIBITED. If you have received
 * this file in error, please immediately notify this to info@wegile.com and destroy
 * the original transmission/files and its attachments without reading or saving them.
 * Thank you.
 */
var cluster = require('cluster'); // initialixing cluster module to run multiple worker processes depending on CPU's attached
if (cluster.isMaster) {
	var numWorkers = require('os').cpus().length;
	for (var i = 0; i < numWorkers; i++) {
		// console.log("");
		cluster.fork(); // Init worker process
	}

	cluster.on('online', function (worker) {
		// console.log("");
		// logs once worker process successfully online
	});

	cluster.on('exit', function (worker, code, signal) {
		// console.log("");
		cluster.fork(); // forcefully restart worker if due to any failure it exit
	});
} else {
	var express = require('express'), // initialixing express module
		http = require('http'),
		helmet = require('helmet'), // initialixing helmet module for security purpose from any hacker threat
		compression = require('compression'), // Compresses the response and improves the performnce of the app
		app = express(),
		jade = require('jade'), // template engine initialized for frontend
		fs = require('fs'); // File system module loaded for File I/O's

	/*
	 * Application Getter/Setter Defined Here
	 * Thses can be used internally 
	 * or user can manually get the specific setter
	 * value any where in the app.
	 */

	app.set('port', process.env.PORT || 3000);
	app.set('views', __dirname + '/views');
	app.engine('jade', jade.__express);
	app.set('view engine', 'jade');


	/*
	 * Application Middleware
	 * this byte of code loads everytime depending  
	 * on the way i want to use it E.g for specific routes
	 * for all the routes in this app. Depending on the scenario
	 * it is executed before my actualy controller logic executed
	 */
	app.use(helmet())
	app.use(compression());
	app.use('/public', express.static(__dirname + '/public', {
		maxAge: 31557600
	}));

	app.use(function (err, req, res, next) {
		if (res.headersSent) {
			return next(err);
		}
		res.status(500);
		res.render('error', {
			error: err
		});
	});

	var middleware = function (req, res, next) {
		req.rootdir = __dirname;
		req.baseurl = req.protocol + '://' + req.headers.host;
		res.setHeader('Content-Type', 'text/html');
		next();
	};
	app.set('middleware', middleware);

	fs.readdirSync('./controllers').forEach(function (file) {
		if (file.substr(-3) == '.js') {
			route = require('./controllers/' + file);
			route.controller(app);
		}
	});

	/*
	 * METHOD: 404 PAGE
	 * ACCEPTS CALLS: GET ONLY
	 * DATABASE INVOLVED: FALSE
	 * STORED PROCEDURE: FALSE
	 * RETURN TYPE: VIEW RENDER FOR 404 PAGE
	 */

	app.get('*', function (req, res) {
		res.render('404', {
			url: req.url
		});
	});

	/*
	 * CREATE SERVER LISTNER FOR APP AT PORT ENV SET
	 */
	http.createServer(app).listen(app.get('port'), function () {
		console.log('Express server listening on port ' + app.get('port'));
	});
}