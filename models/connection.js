/*
 * COPYRIGHT SOURCE CODE &COPY; RUPINDER SINGH SETHI M: +91-9915455521
 *
 * PLATFORM: NODEJS/EXPRESS/LOKIJS
 *
 * DEVELOPER NAME: RUPINDER SINGH SETHI / SR. NODE-JS/ANGULAR-JS/WEBRTC/SOCKET-IO/FULL STACK DEVELOPER
 *
 * DEVELOPER PROFILE: https://in.linkedin.com/in/rup-sethi-5753a3b4
 *
 * LOCATION: CHANDIGARH - INDIA
 *
 * DATE: 22 JUNE 2016
 *
 * CONFIDENTIALITY NOTICE: This source code file contains confidential information
 * that is legally privileged. Do not copy/re-use/re-distribute this file. If you
 * are not the intended recipient/user of this file, any disclosure, copying, or use
 * of any of the transmitted information is STRICTLY PROHIBITED. If you have received
 * this file in error, please immediately notify this to info@wegile.com and destroy
 * the original transmission/files and its attachments without reading or saving them.
 * Thank you.
 */

var loki = require('lokijs'), // Database module loaded to the app
	db = new loki('dreamlinesdb.json', {   // initilizing userdefined database for the APP use
		autoload: true,
		autoloadCallback: loadHandler  // Callback when DB initialized every time application restarted
	});   

/*
* METHOD: Initializing Database collection
* ACCEPTS CALLS: POST ONLY
* DATABASE INVOLVED: TRUE
* INPUT PARAMETERS: CSV DATA
* RETURN TYPE: Collection instance loaded with CSV data
*/
function loadHandler() {
	var collection = 'airports';
	db.removeCollection(collection);
	var airports = db.getCollection(collection);
	if (!airports)
		airports = db.addCollection(collection);

	if (airports.count() == 0) {
		console.log("Dataloaded For Worker Process!!!");
		var Converter = require("csvtojson").Converter; // CSV Reader/parser loaded
		var converter = new Converter({
			constructResult: false   // Set to false for big amount of CSV data
		});  
		require('fs').createReadStream("./airport.csv").pipe(converter);  // Readeing CSV to map key/values
		//end_parsed will be emitted once parsing finished
		converter.on("record_parsed", function (jsonObj) {
			airports.insert(jsonObj);   // Importing data to collection
		});
	}
	db.saveDatabase();  // Save all the above change done on DB
}

module.exports = db;